// struct for card games

#include <iostream>
#include <conio.h>

enum Rank // ace high
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

enum Suit
{
	SPADE, DIAMOND, CLUB, HEART
};

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{
	// testing

	Card card1;
	card1.rank = SIX;
	card1.suit = HEART;

	Card card2;
	card2.rank = QUEEN;
	card2.suit = SPADE;

	_getch();
	return 0;
}
